# SIGer

#### 介绍
青少年开源文化的阵地 - SIGer 兴趣小组（科普期刊）

这个仓会汇集各种兴趣小组的文件，想法。采用 SIG 管理架构，通过 PR 分享各自社团的学习成果。寄希望成为一个学生社团管理的宝典和百科。

同时，所有的 SIGer 兴趣小组，都应该具备数字化工具，专属工具应该被推荐，必定每个小组有自己的特点。通用工具肯定是必备的。协同创作本身，就符合开源社区的标配。而 SIGer 使用的起点工具 GITEE，只是一个脚手架的作用，未来一定会被全新的工具所替代。就如 LINUX 之于 minux.

#### 本期 

《[ **开源治理的根基** 是教育和赋能！](第14期%20开源治理的根基是教育和赋能！.md)》 #14th

- _**[教育](./%E4%B8%93%E8%AE%BF/%E3%80%90RMS%E3%80%91Free%20Software,%20Free%20Society.md#%E6%95%99%E8%82%B2)**_ 即指明方向，指北就是文化选择的问题，要什么样的生活的问题。  
-  **[赋能](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)**  是赋能未来，赋能青少年，去肩负创造未来美好生活的使命，即理想和信念。

#### 优秀

《[全栈学习平台 —— 可编程个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)》 ——  **石榴派**   
[The Personal Programmable Computer - Shiliu Pi 99](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)

- 石榴派 漂流计划 Shiliu Pi (Pomegranate Pi) Drift Plan. 正在进行时 ...
- 祺福“石榴派”早日装备到火种志愿者手中。为万千少年带去欢乐！

 **《[Better RISC-V For Better Life](第4期%20Better%20RISC-V%20For%20Better%20Life.md)》#4th** 

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/124434_b68e4799_5631341.png "屏幕截图.png")](第4期%20Better%20RISC-V%20For%20Better%20Life.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/204922_166bcbeb_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0715/075956_be1fb9ad_5631341.jpeg)

> @[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md) @[PicoRio](RISC-V/PicoRio.md) @[浪潮RISC-V小组](RISC-V/riscv.md) @[RV与芯片评论](RISC-V/rvnews.md) @[CRVA](RISC-V/crvs2020.md) @[RV4Kids](RISC-V/RV4Kids.md) 依照本期 SIGer 专刊的 《[开源芯片 RISC-V](https://gitee.com/yuandj/siger/issues/I3B6OG) 》的主题，一篇一篇 md 入库。[JOHN L. HENNESSY DAVID A. PATTERSON](RISC-V/JOHN-L-HENNESSY-DAVID-A-PATTERSON.md) [COMPUTER ARCHITECTURE](RISC-V/a-new-golden-age-for-computer-architecture.md) 这些关键字映入眼帘，没有比这个时刻更加激动人心的啦，一个崭新的[黄金时代](RISC-V/a-new-golden-age-for-computer-architecture.md)到来啦。

 **[RV4Kids](RISC-V/RV4Kids.md)** ：  _中文名（[ **RV少年** ](https://gitee.com/RV4Kids)）_ 

我极力地推荐 [RV4Kids](RISC-V/RV4Kids.md) 这个崭新的教育品牌，是我和 [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 主编[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)先生共同推荐的一个科普项目，我们邀请 RISC-V 的全体同仁和社区都能展开双臂拥抱青少年，您只需在您的实践项目（开源项目）中 @[RV4Kids](https://gitee.com/RV4Kids) 字样，尽您所能，在您开展的 RISC-V 相关的活动，课程，工作坊，等等可以吮吸 RISC-V 知识的场合，为青少年提供便利，允许他们参与，安静地来，悄悄地走，积极提问，求知若渴。谁能知道，不远的将来，他们中的某人会成为您的同事，甚至战友。“ **身负强国使命，与一众小伙伴各种厮杀** ” 呢？@小孔语录

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/193300_6ee28677_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/193244_7327bba1_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)

> 学习小组 是 SIGer 的延伸，以 RISCV 为学习目标，分享实验课心得，成为 [RV4kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) 的主要内容，编辑本身就是学习。这符合 SIGer 编委的工作和学习内容。唯有分享，才是最好的学习方法。创刊后，以青少年科普新势力受邀 CNRV 中国峰会，产出两期：2《[Shiliu Si 石榴核](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)》，3《[中国峰会 日记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)》。并伴随暑期 石榴派实验课的启动，4《[一生一芯](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)》5《[FPGA 学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)》等专题陆续登场。锁定 6《[全栈学习平台——可编程个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)》后，将学习小组的学习方法沉淀为 7《[IC WIKI](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)》的学习笔记的知识分享。将 8《[SIGer 编辑手册](专访/【石榴派】SIGer%20编辑手册.md)》纳入学习小组序列，一共八期，统一风格，见 [增刊](#%E5%A2%9E%E5%88%8A)。

#### 往期

- [第14期 开源治理的根基是教育和赋能！](第14期%20开源治理的根基是教育和赋能！.md) (2021.7.31)
- [第10期 南征北战](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md) [上](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)/[中](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%AD3%EF%BC%89.md)/[下](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md) (2021.5.11)
- [第4期 Better RISC-V For Better Life](第4期%20Better%20RISC-V%20For%20Better%20Life.md) (2021.3.21)
- [第1期 迎接我们未来的生活方式](第1期%20迎接我们未来的生活方式.md) (2021.3.5)
- [第0期 Hello, openEuler! 拥抱未来](第0期%20Hello,%20openEuler!%20拥抱未来.md) (2021.2.3)

#### 增刊

 [![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/184946_a3e91e72_5631341.png "屏幕截图.png")](https://gitee.com/yuandj/siger/issues/I3MXUP)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/184657_c70e325c_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)

- 青春主题，完全由 “SIGer” 执行编委 主笔，呼应 “缘起”。
- [RV4Kids](https://gitee.com/RV4Kids/) 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)》 - [RV 一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)  
  （[1](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)/[2](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)/[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)/[5](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)/[6](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)/[7](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)/[8](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E7%9F%B3%E6%A6%B4%E6%B4%BE%E3%80%91SIGer%20%E7%BC%96%E8%BE%91%E6%89%8B%E5%86%8C.md)/[9](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md)/[10](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/issue%2010%20-%20inspur.md)/[11](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md)/[12](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2012%20-%20TurboLinux.md)/[13](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%20Issue%2013%20-%20Codasip.md)/[14](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014%20-%20Difftest%20Learning.md)/[15](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2015%20-%20Ubuntu%20heart.md)/[16](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2016%20-%20W%20R%20GNUgamer.md)） - [RV 少年](https://gitee.com/RV4Kids)

#### 番外

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0330/095806_af12450a_5631341.png "屏幕截图.png")](./%E7%AC%AC9%E6%9C%9F%EF%BC%9A%E6%88%91%E4%BB%AC%E7%9A%84%E6%9C%AA%E6%9D%A5%E6%98%AF%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7%EF%BC%81.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0330/094133_35970012_5631341.png "屏幕截图.png")](https://talk.quwj.com/topic/2119)

最近喜欢 B站 的学习氛围，也来个二次元，SIGer 本不分内外，本番意在连接，上荐语：

- 趣派社区是我的称呼，我从那里知道了 “考古”，推荐体验：图床、B站播报
- MARS 就不多说了：“社区的同频共振，认同并实践「分享创造连接 协作产生价值」”

#### 贡献

1.  Fork 本仓库
2.  新建 siger xxx 分支 （xxx为您选择的兴趣小组的名字sig，任何分支，请务必指修改一个SIG目录）
3.  提交代码 （这里是您贡献的新资料，或者代码。）
4.  新建 Pull Request 
   （请在PR前，一定要保存好自己的本地更新，并在此之前，对master的版本变更，做到心中有数）

详见：[how to become a SIGer? 编委是如何工作的？](%E7%AC%AC1%E6%9C%9F%20%E8%BF%8E%E6%8E%A5%E6%88%91%E4%BB%AC%E6%9C%AA%E6%9D%A5%E7%9A%84%E7%94%9F%E6%B4%BB%E6%96%B9%E5%BC%8F.md#4-%E4%BB%BB%E5%8A%A1how-to-become-a-siger-%E7%BC%96%E5%A7%94%E6%98%AF%E5%A6%82%E4%BD%95%E5%B7%A5%E4%BD%9C%E7%9A%84%E5%BC%80%E6%BA%90%E7%A7%91%E6%99%AE-siger-1%E6%9C%9F-%E6%8E%A8%E5%B9%BF%E6%96%B9%E6%A1%88-%E9%A1%BA%E5%88%A9%E5%AE%9E%E6%96%BD)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/094640_bdeeac29_5631341.png "屏幕截图.png")

附：《[SIGer 编辑手册](专访/【石榴派】SIGer%20编辑手册.md)》2021-7-20

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/201609_1d0257c0_5631341.png "屏幕截图.png")](专访/【石榴派】SIGer%20编辑手册.md)

#### 伴礼

出游返乡，拜访故友，总不可空手去空手回，礼尚往来自古有之。  
访友皆是客，回礼相送，均为自制，请笑纳。  
附教程，供您使用，只愿将开源文化分享的更远更广。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/085600_511ffead_5631341.png "213030_ac45c7da_5631341副本.png")](https://gitee.com/shiliupi/shiliupi99)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/230721_16a40023_5631341.png "屏幕截图.png")](https://gitee.com/mixshare/occupymars) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/230844_0b125c03_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/caihongqiao)


#### 吾愿

- 祺福天下孩童，健康快乐！ :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray: 

  > 请您支持，共同祈愿，分享寄语：  
为自己，为家人，为朋友，为他人，为世界和平，均可。  
伴以的捐赠，请包含数字 3/6/9 ，请相信您的愿力，求天下与您共同见证。 :pray: 
