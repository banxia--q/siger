# [13-Year-Old, Nicholas Sharkey, Creates a RISC-V Core](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4562007_link)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/091225_7f3c2618_5631341.png "屏幕截图.png")
https://riscv.org/blog/2020/11/13-year-old-nicholas-sharkey-creates-a-risc-v-core/

By [Steve Hoover](https://riscv.org/author/stevehoover/)   November 13, 2020  

One of my great pleasures operating my EDA startup, Redwood EDA, is working with enthusiastic college students and open-source developers and seeing how our technology is renewing excitement for logic design. Recently, Kunal Ghosh of VLSI System Design and I conducted our third “Microprocessor for You in Thirty Hours” (MYTH) Workshop, where participants learn about RISC-V and build their own RISC-V CPU cores (something that’s typically done over the course of a semester or two). In addition to reaching graduate students and professionals, one of our goals with the workshop is to give students an opportunity to learn logic design earlier in their education–like, say, as a college freshman or sophomore. So just imagine our surprise when Nicholas introduced himself in the chat forum of our workshop.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/091541_e7cdcb96_5631341.png "屏幕截图.png")

This was a real test of our goals. It was also a real testament to Nicholas’s thirst for knowledge and the outside-the-box thinking of his home-schooling parents, Rasa and Mike. Having a 13-year-old of my own, I was particularly impressed by Nicholas’s willingness to put himself out there, asking questions and joining Zoom calls (not to mention his familiarity with Linux). I’ve since learned that Nicholas has been awarded in spelling bees and math competitions and is an expert at solving the Rubik’s Cube. Somehow, I’m not surprised.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/091602_e3b4041a_5631341.png "屏幕截图.png")

Admittedly, the workshop was a stretch for Nicholas and, for him, did not live up to its 30-hour claim. At the close of the workshop, he had gotten a little more than halfway through. He had learned about the RISC-V ISA and compilation tools; he had developed circuit design skills; he had created a pipelined calculator circuit; and his first RISC-V CPU was showing signs of life. We considered this a great success!

But Nicholas, after working five long days straight in the workshop, still had the stamina to see his project through. (Note that when I suggest to my own sons that they design a circuit with me, it tends to be viewed as more of a homework assignment, and I’m lucky if they spend an hour with me.) Normally, when the workshop ends, we shut it down, and work stops, but we agreed to a bit of special treatment, and Nicholas kept at it in the evenings, after returning to his normal schooling.

Given his enthusiasm, I figured Nicholas would at least be able to get through the day-4 content and see his non-pipelined CPU summing numbers from 1 to 9. That would even be blog-worthy. With continued online chatter and a Zoom call or two, he did complete the day-4 CPU!

Day 5, on the other hand, would be a bit much to ask. Day 5 is where we really see how well students absorbed what they were taught during days 1-4. On day 5, students are asked to pipeline their CPU, dealing with various pipeline hazards. That’s a bit hard-core for a 13-year-old, right?

![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/091614_c97d036c_5631341.png "屏幕截图.png")

Having reviewed his work and discussed it with him, I’m happy to say that Nicholas has indeed successfully completed his 5-stage pipelined RISC-V CPU core and will be getting his certificate soon!

When I asked about his experience, he responded (perhaps with a bit of parental oversight) “I enjoyed the challenge very much, and it has gotten me excited about RISC-V and digital design.” He also expressed his gratitude to Shivam Potdar and the rest of the MYTH Workshop crew.

Hats off to you, young Nicholas, circuit designer extraordinaire. Keep thirsting for knowledge. It will take you far!

 _The 4th run of the MYTH Workshop starts December 2nd. I wouldn’t necessarily recommend signing up your children, but if you are interested in participating, we would love for you to join us. Just be aware that, if you do [sign up](https://www.vlsisystemdesign.com/riscv-based-myth/), the pressure is on. You wouldn’t want to be outdone by a middle-schooler. Or… perhaps you already have and need to catch up ;). Also, be on the lookout for similar training from [RISC-V Learn Online](https://riscv.org/community/learn/risc-v-learn-online/), coming soon!_ 

