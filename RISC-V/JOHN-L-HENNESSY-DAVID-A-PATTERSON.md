david patterson and john hennessy  
computer organization and design 5th edition morgan kaufmann 2014

GOOGLE 搜索推荐，这两位并列出现，hennessy 另外单表。看到这，这是写教科书的祖师爷啦。 :pray:  
（正文全文等待译文校对后，再行刊载，本篇先抛砖引玉，对祖师爷予以介绍）

## a new golden age for computer architecture
https://cacm.acm.org/magazines/2019/2/234352-a-new-golden-age-for-computer-architecture/fulltext

By  **John L. Hennessy** , David A. Patterson  
Communications of the ACM, February 2019, Vol. 62 No. 2, Pages 48-60  
10.1145/3282307

We began our Turing Lecture June 4, 201811 with a review of computer architecture since the 1960s. In addition to that review, here, we highlight current challenges and identify future opportunities, projecting another golden age for the field of computer architecture in the next decade, much like the 1980s when we did the research that led to our award, delivering gains in cost, energy, and security, as well as performance.

"Those who cannot remember the past are condemned to repeat it."George Santayana, 1905

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/222753_2db7b91b_5631341.png "屏幕截图.png")

Figure. Features of four models of the IBM System/360 family; IPS is instructions per second.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/223011_479196f3_5631341.png "屏幕截图.png")

Figure 1. University of California, Berkeley, RISC-I and Stanford University MIPS microprocessors.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/223032_4bcede5c_5631341.png "屏幕截图.png")

Figure 8. Functional organization of Google Tensor Processing Unit (TPU v1).

计算机体系结构的新黄金时代

约翰·轩尼诗（John L.Hennessy），大卫·帕特森（David A.Patterson）  
ACM通讯，2019年2月，第1卷。 62第2页，第48-60页

我们从2018年6月4日开始图灵演讲，回顾了自1960年代以来的计算机体系结构。除了该评论之外，在这里，我们重点介绍当前的挑战并确定未来的机会，并预测下一个十年计算机架构领域的又一个黄金时代，就像1980年代我们进行研究并获得奖项的成就一样，它为我们带来了丰硕的成果。成本，能源，安全性以及性能。

“那些不记得过去的人会被谴责以重蹈覆辙。”乔治·桑塔亚那（George Santayana），1905年

输入图片说明  
数字。 IBM System / 360系列的四种型号的功能； IPS是每秒的指令。

输入图片说明  
图1.加州大学伯克利分校的RISC-I和斯坦福大学的MIPS微处理器。

输入图片说明  
图8. Google Tensor处理单元（TPU v1）的功能组织。

## 约翰·轩尼诗（John Hennessy）和大卫·帕特森（David Patterson）荣获2017年ACM AM图灵奖

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/224034_2a405bfd_5631341.png "屏幕截图.png")

ACM任命斯坦福大学前校长John L. Hennessy和加州大学伯克利分校退休教授David A. Patterson荣获2017年ACM AM图灵奖，以表彰其开创了系统，定量的设计和评估方法对微处理器行业产生持久影响的计算机体系结构。

他们在6月4日举行的ISCA会议上发表了图灵演讲。  
https://www.acm.org/hennessy-patterson-turing-lecture

约翰·轩尼诗（John Hennessy）和大卫·帕特森（David Patterson）在ISCA 2018上发表图灵演讲  
2017年ACM AM图灵奖获得者John Hennessy和David Patterson于6月4日在洛杉矶的ISCA 2018上发表了图灵演讲。讲座于美国太平洋夏令时间下午5点至下午6点举行，并向公众开放。演讲视频可以在下面查看。

主题为“计算机体系结构的新黄金时代：特定领域的硬件/软件协同设计，增强的安全性，开放指令集和敏捷芯片开发”，演讲涵盖了计算机体系结构的最新发展和未来方向。

轩尼诗和帕特森因“开创了一种系统，定量的方法来设计和评估对微处理器行业产生持久影响的计算机体系结构而被授予图灵奖”。

## 大卫·帕特森（David Patterson）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/225048_c058d04a_5631341.png "屏幕截图.png")

### 获奖者

大卫·帕特森（David Patterson）  
- ACM AM图灵奖（2017）  
- ACM-IEEE CS Eckert-Mauchly奖（2008）  
- ACM杰出服务奖（2007）
- ACM研究员（1994）
- ACM卡尔·卡尔斯特罗姆杰出教育家奖（1991）
- 2017 ACM AM图灵奖

### ACM AM图灵奖
美国-2017  
阅读完整的引文和文章  
引文

用于开创一种系统的，定量的方法来设计和评估对微处理器行业产生持久影响的计算机体系结构。

大卫·帕特森（David Patterson）和约翰·轩尼诗（John Hennessy）创建了一种系统，定量的方法来设计更快，功耗更低，复杂度更低的微处理器。他们的方法导致了持久且可重复的原则，几代建筑师已在学术界和工业界的许多项目中使用了这些原则。其影响是惊人的：数百亿个处理器使用了降低复杂性的架构。特别是，几乎为所有智能手机供电的ARM处理器受到了Patterson和Hennessy的工作的极大影响。

帕特森和轩尼诗分别从斯坦福大学和加州大学伯克利分校的颇具影响力的MIPS和RISC处理器上的应用工作中学到了知识，然后表达了他们使用名为DLX的参数化体系结构进行设计的方法。有了它，他们为评估集成系统提供了一个智力上简单，健壮和定量的框架。

他们对集成的洞察力在关键方面证明了范式的转变。例如，他们指导设计人员仔细优化他们的系统，以考虑不同的内存和计算成本。他们的工作还实现了从寻求原始性能到设计架构的转变，该架构提供了放置在单个芯片上的元素的正确平衡，从而解决了诸如功耗，散热和芯片外通信之类的问题。

Patterson和Hennessy在非常有影响力的书《计算机体系结构：一种定量方法》中整理了他们的见解，吸引了采用并进一步发展其思想的几代工程师和科学家。确实，他们的工作巩固了我们对新处理器的体系结构进行建模和分析的能力，从而极大地加快了微处理器设计的进步。

比尔·盖茨（Bill Gates）评估了帕特森（Patterson）和轩尼诗（Hennessy）的工作所释放的力量及其影响，这证明了他们的贡献“已被证明是整个行业蓬勃发展的基础。”

为了创建用于设计计算机体系结构的有原则的科学框架，并为其工作的范式转变性质，选择了Patterson和Hennessy来获得图灵奖授予的唯一认可。

 **新闻稿：** 
https://awards.acm.org/binaries/content/assets/press-releases/2018/march/turing-award-2017.pdf

## 约翰·轩尼诗
https://awards.acm.org/award-winners/hennessy_1426931

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/224746_0708cde9_5631341.png "屏幕截图.png")

### 获奖者
约翰·轩尼诗
- ACM AM图灵奖（2017）
- ACM-IEEE CS埃克特-莫赫利奖（2001）
- ACM研究员（1997）
- 2017 ACM AM图灵奖

### ACM AM图灵奖